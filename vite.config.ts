import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vite';
import wasm from 'vite-plugin-wasm';

export default defineConfig({
	plugins: [sveltekit(), wasm()],
	optimizeDeps: {
		// This is necessary because otherwise `vite dev` includes two separate
		// versions of the JS wrapper. This causes problems because the JS
		// wrapper has a module level variable to track JS side heap
		// allocations, initializing this twice causes horrible breakage
		exclude: ["@automerge/automerge-wasm"]
}
});
