export interface FormatSpec {
  locale: string
  dateFormat: string
  monthHeader: string  
  nothingAvailable: string
  availableFromFor: string
  minimumPrice: string
  maximumPrice: string
  additionalPersonPrice: string
  minNumberOfNights: string
  formatSeating: string
  numberOf: string
  size: string
  bed: string
}

export interface Formats {
  [lang: string]: FormatSpec
}