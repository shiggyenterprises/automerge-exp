import { writable, derived, get } from "svelte/store";

import * as Automerge from '@automerge/automerge'
import type { DecodedChange, Doc } from '@automerge/automerge'
import type { SupabaseClient } from "@supabase/supabase-js";

import type { SiteConfig, SiteConfigDTO } from './site'
import type { I18n } from "./i18n";
import type { Acco } from "./accos";
import type { CookieSettings } from "./cookies";
import type { Fonts } from "./fonts";
import type { Formats } from "./formats";
import type { Settings } from "./settings";
import type { Nav } from './nav';
import type { Landing } from './landing';


// this store the current version of the config
const _editing = writable<Doc<SiteConfig>>()
export const editing = derived(_editing, (s) => s)

// this stores the config at it's published state
const _live = writable<Doc<SiteConfig>>()
export const live = derived(_live, (s) => s)

//export changes, between live and editing ... needs to save ...
export const changes = derived([_editing, _live], (values):DecodedChange[] => {
  const changes = Automerge.getChanges(values[0], values[1])
  if(changes && changes.length > 0) {
    return changes.map( (c) => Automerge.decodeChange(c) )
  }
  return []
})

// state ... is the config loaded ... from anywhere
const _loaded = writable<boolean>(false)
export const loaded = derived(_loaded, (s) => s)

const _dirty = writable<boolean>(false)
export const dirty = derived(_dirty, (s) => s)

const _nextAutosaveIn = writable<number>(0)
export const nextAutosaveIn = derived(_nextAutosaveIn, (s) => s)

/*
Dealing with siteConfig state:

start with an "empty" siteConfig (NOT the version from GitHub) ... the Github version is not important anymore: it's just the result ... no syncing!!!

1) "initial"

1.1) Landing

pretty much empty ... or?!? ... this is not so nice ux wise.

problem: missing location information to provide some basic 'blocks' like weather or a town description ... I could do something here ... like prepare some things: Weather Block with some towns and there 'coordinates' preselected ... predefined texts describing the town ... a generic welcome text and such.

I could just leave the things in there that are "generic" enough? Not so nice either.

1.2) Acco[]

this is interesting ... what needs to be setup to generate a acco site ... at least an active acco! what blocks are there by default ...

1.3) Nav, just the basics

1.4) i18n, the core translations, for all the languages ... language config stays separated?!?

1.5) Formats, Fonts, Settings, CookieSettings ... do it later 

TODO 1.6) Missing global.css for colors and custom .ico file , etc.


2) ""


-----
What states do I really need!?! And what do they say ... this is kinda tricky ...

I need "publishable/not publishable" ... what makes it publishable?:
- complete translations!? ... if there are missing some, is this really a big problem, 

TODO I could/should implement a fallback to default lang, if there is something missing!!!

- multiple "active" accos?!? ... is this truly the case!?
- type validations for the different parts, can this really happen ... and if yes should I care?!?

I need "there are not published changes": simple, but it's actually obvious from comparing live with editing doc version!!



*/



/*
When and what to store in Supabase:

- store the currently live document, why? -> to be able to show the differences between the editing doc and the live doc => must have, but does the head of live suffice, probably

- store the editing doc, why? -> to be able to pick up from a different device => should have, maybe with delay ... save every 10s at most ... or something?!?

- store the initial document, why should I? => probably unnecessary
*/

/*
What and when to store a snapshot in localStorage

- store the edit doc in localStorage on every edit
- is that it?!?

*/

/*
Why 'automerge' ... 

- Change History
- Undo/Redo somehow ... making a past state the current version ...
- Optimized storage
- Potentially p2p sync, not important now

*/


/*
Questions:

- Undo/Redo changes
- When & why to fork/branch out?


*/


/*
load config

1) try from local storage
if no: try to load it from supabase (can I assume it's in sync with live version?)

if no: fetch it from github

if no: use default

*/

export const loadConfig = async (userID:string, supabase:SupabaseClient) => {
  if(get(_loaded)) return

  let binary = localStorage.getItem(`editing-${userID}`)
  if(binary) {
    let doc = Automerge.load<SiteConfig>(binary.split(',') as any as Uint8Array)
    if(doc) {
      _editing.set(doc)
      _live.set(doc)
      _loaded.set(true)

      return
    }
    else {
      console.log('something went wrong')
    }
  }

  //will return only one ... every user can only have one site config
  //XXX what about 'different' docs in supabase and local ... check for sync?!?
  const { data, error } = await supabase.from('site_config').select('*').returns<SiteConfigDTO[]>()
  if(error) {
    console.log('Unable to read from Supabase:', error)
  }
  // siteConfig found in supabase but not locally ... meaning: new client/browser/incognitoWindow
  if(data && data.length > 0) {
    console.log(data)
    
    const d = data[0]
    let editingConf = Automerge.load<SiteConfig>(d.editing.split(',') as any as Uint8Array)
    let liveConf = Automerge.load<SiteConfig>(d.live.split(',') as any as Uint8Array)

    if(liveConf) {
      _live.set(liveConf)
      _editing.set(editingConf)
      _loaded.set(true)
      return
    }
    else {
      console.log('something went wrong')
    }
  }

  //no site config locally and in supabase ... so probably in github repo,
  //XXX not doing this here ... initialization of could/should be done when the  
  throw Error('SiteConfig not initialized')

}

export const initConfig = async (userID: string, config:SiteConfig, supabase:SupabaseClient) => {

  let doc = Automerge.change<SiteConfig>(
    Automerge.init<SiteConfig>(), 
    (d:SiteConfig) => {
      d.accos = [ ...config.accos ]
      d.cookies = { ...config.cookies }
      d.fonts = { ...config.fonts }
      d.formats = { ...config.formats }
      d.i18n = { ...config.i18n }
      d.landing = { ...config.landing }
      d.nav = { ...config.nav }
      d.settings = { ...config.settings }
    })

  // set local store to initial config
  _editing.set(doc)
  _live.set(doc)
  _loaded.set(true)


  let binary = `${Automerge.save(doc)}`

  //store in supabase after initialization
  const dto:SiteConfigDTO = {
    user: userID,
    created: (new Date()).toISOString(),
    updated: (new Date()).toISOString(),
    live: binary,
    editing: binary
  }
  supabase.from('site_config').upsert(dto)

  //store binary locally
  localStorage.setItem(`editing-${userID}`, binary)

  return
}

let serverMissCounter = 0;
let lastServerSync = new Date();
const TIMEOUTS = [30, 60, 300, 600, 1200, 1800];

const debounceServerSave = async (supabase:SupabaseClient) => {
  
  const updFun = async () => {
    if(get(_dirty)) {
      const now = new Date().getTime()
      const timeout = serverMissCounter < TIMEOUTS.length ? TIMEOUTS[serverMissCounter] : TIMEOUTS[ TIMEOUTS.length - 1 ]
      const then = lastServerSync.getTime() + (timeout * 1000)
      
      const itsTime = then < now
      const diff = Math.floor((then - now) / 1000)
      _nextAutosaveIn.set( diff > 0 ? diff : 0 ) 
      
      if(itsTime) {
        //get the current state from supabase
        const { data: selectData, error: selectError } = await supabase.from('site_config').select('*').returns<SiteConfigDTO[]>()
        if(selectError || !selectData || selectData.length !== 1) {
          console.log('Unable to read current state from Supabase:', selectError)
          serverMissCounter++

          setTimeout(updFun, 1000)
          return
        }

        let upd:SiteConfigDTO = selectData[0]
        upd.updated = (new Date()).toISOString()
        upd.editing = `${ Automerge.save(get(_editing)) }`

        const { error: upsertError } = await supabase.from('site_config').upsert(upd).select()
        if(upsertError) {
          console.log('Unable to upsert current state to Supabase:', upsertError)
          serverMissCounter++

          setTimeout(updFun, 1000)
          return
        }

        lastServerSync = new Date();
        _dirty.set(false);
        serverMissCounter = 0;
      }
    }
    else {
      setTimeout(updFun, 1000)
    }
  }

  updFun()
}

const save = async (userID: string, supabase:SupabaseClient) => {
  // get binary representation of automerge doc
  const editingBinary = `${Automerge.save(get(_editing))}`
  localStorage.setItem(`editing-${userID}`, editingBinary)

  return debounceServerSave(supabase)
}

const update = async (changeFun:(doc:SiteConfig)=>void, supabase:SupabaseClient) => {
  const { data, error } = await supabase.auth.getUser()
  if(error) {
    console.log('Supabase auth error:', error)
    throw new Error('Not authorized')
  }

  const e = get(_editing)
  const u = Automerge.change(e, changeFun)
  _editing.set(u)


  const changes = Automerge.getChanges(e, u)
  if(changes && changes.length > 0) {
    _dirty.set(true)
    save(data.user.id, supabase)
  }
}

export const updateNav = (nav:Nav, supabase:SupabaseClient) => {
  update( (doc:SiteConfig) => {
    doc.nav = nav
  }, supabase)
}


export const updateLanding = (l:Landing, supabase:SupabaseClient) => {
  update( (s:SiteConfig) => {
    s.landing = l; 
  }, supabase)
}


export const updateI18n = (i:I18n, supabase:SupabaseClient) => {
  update( (s:SiteConfig) => {
    s.i18n = i; 
  }, supabase)
}


export const updateAccos = (a:Acco[], supabase:SupabaseClient) => {
  update( (s:SiteConfig) => {
    s.accos = a; 
  }, supabase)
}


export const updateCookies = (c:CookieSettings, supabase:SupabaseClient) => {
  update( (s:SiteConfig) => {
    s.cookies = c; 
  }, supabase)
}


export const updateFonts = (f:Fonts, supabase:SupabaseClient) => {
  update( (s:SiteConfig) => {
    s.fonts = f;
  }, supabase)
}

export const updateFormats = (f:Formats, supabase:SupabaseClient) => {
  update( (s:SiteConfig) => {
    s.formats = f; 
  }, supabase)
}

export const updateSettings = (set:Settings, supabase:SupabaseClient) => {
  update( (s:SiteConfig) => {
    s.settings = set; 
  }, supabase)
}


export const publish = async ( pubConfig:(userID:string, siteConfig:SiteConfig)=>Promise<boolean>,  supabase:SupabaseClient):Promise<boolean> => {

  try {

    const { data, error } = await supabase.auth.getUser()
    if(error) {
      console.log('Supabase auth error:', error)
      throw new Error('Not authorized')

    }

    const newSiteConfig = get(_editing)

    //TODO figure out ... this has to become an API call
    const success = await pubConfig(data.user.id, newSiteConfig)

    if(!success) {
      console.log('Failed to upload siteConfig to git repo.')
      return false
    }

    const { data: selectData, error: selectError } = await supabase.from('site_config').select('*').returns<SiteConfigDTO[]>()
    if(selectError || !selectData || selectData.length !== 1) {
      console.log('Unable to read current state from Supabase:', selectError)
      return false
    }

    let upd:SiteConfigDTO = selectData[0]
    upd.updated = (new Date()).toISOString()
    upd.live = `${ Automerge.save(newSiteConfig) }`

    const { error: upsertError } = await supabase.from('site_config').upsert(upd).select()
    if(upsertError) {
      console.log('Unable to upsert current state to Supabase:', upsertError)
      return false
    }

    _live.set(newSiteConfig)
    return true
  }
  catch(e) {
    console.log('Error publishing new config', e)
    return false
  } 
}





/*

export const initialDocument = async (userID:string):Promise<SiteConfig|null> => {
  
  // get the document from GitHub if not stored in Supabase ... clean it up on the process?!?
  // transform it into a Automerge Doc
  const doc = Automerge.init<SiteConfig>()
  const s = DefaultSiteConfig
  return s
}


export const liveDocument = async (userID:string):Promise<SiteConfig|null> => {
  
  return null
}

export const change = async (userID:string, changeFun:(d:SiteConfig)=> void) => {

  Automerge.change(get(_currentDoc), changeFun)
}


export const changes = async (userID:string):Promise<Change[]|null> => {

  return null
}

*/


/*
import { writable, derived, get } from "svelte/store";

import { mapRenderSite, type SiteConfig } from "$lib/components/site/types/site";
import type { Nav } from '$lib/components/site/types/nav';
import type { Landing } from '$lib/components/site/types/landing';

import type { Lang, LangSettings } from '@accompa/types';


import type { RenderSiteDTO, RenderSite } from "$lib/components/site/types/site";
import type { SupabaseClient } from "@supabase/supabase-js";
// @ts-ignore
import { deepEqual } from '@accompa/helpers';
import {Languages, type  I18n } from "$lib/components/site/types/i18n";
import type { Acco } from "$lib/components/site/types/accos";
import type { CookieSettings } from "$lib/components/site/types/cookies";
import type { Fonts } from "$lib/components/site/types/fonts";
import type { Formats } from "$lib/components/site/types/formats";
import type { Settings } from "$lib/components/site/types/settings";


const renderSiteLoadedStore = writable<boolean>(false);
const renderSiteStore = writable<RenderSite>();
export const renderSite = derived( renderSiteStore, e => e)
export const renderSiteLoaded = derived(renderSiteLoadedStore, e => e)

export const loadRenderSite = async(supabase:SupabaseClient) => {
  
  renderSiteLoadedStore.set(false)
  const { data, error } = await supabase.from('sites').select().returns<RenderSiteDTO[]>()

  if(error) {
    console.log('Error getting data:',error)
  }
  if(data && data.length == 1) {
    renderSiteLoadedStore.set(true)
    const renderSite = mapRenderSite(data[0])
    renderSiteStore.set(renderSite)
  }
}

const langSettingsLoadedStore = writable<boolean>(false);
const langSettingsStore = writable<LangSettings>({
  defaultLang: 'en',
  user: '',
  activeLangs: ['en', 'de']
});
export const langSettingsLoaded = derived(langSettingsLoadedStore, e => e)
export const langSettings = derived( langSettingsStore, e => e)

export const loadLangSettings = async (supabase:SupabaseClient) => {
  langSettingsLoadedStore.set(false)
  const { data, error } = await supabase.from('lang_settings').select().returns<LangSettings[]>()

  if(error) {
    console.log('Error loading', error)
  }

  if(data && data.length == 1) {
    langSettingsLoadedStore.set(true)
    langSettingsStore.set(data[0])
  }

}

const defaultFonts = {
  "main": {
    "shortName": "helvetica",
    "cssName": "Helvetica",
    "variant": "normal",
    "fallback": "sans-serif",
    "fontsource": false
  },
  "nav": {
    "shortName": "system-ui",
    "cssName": "system-ui",
    "variant": "small-caps",
    "fallback": "sans-serif",
    "fontsource": false
  },
  "pageTitle": {
    "shortName": "arizonia",
    "cssName": "Arizonia",
    "variant": "normal",
    "fallback": "sans-serif",
    "fontsource": true
  },
  "landingTitle": {
    "shortName": "raleway",
    "cssName": "Raleway",
    "variant": "normal",
    "fallback": "sans-serif",
    "fontsource": true
  },
  "header": {
    "shortName": "carrois-gothic",
    "cssName": "Carrois Gothic",
    "variant": "small-caps",
    "fallback": "sans-serif",
    "fontsource": true
  } 
}

export const defaultSiteConfig:SiteConfig = {
  nav: {main: [], footer: []},
  landing: {headerPhoto: "", sections: []},
  i18n: {defaultLang: "en", translations: {}},
  accos: [],
  cookies: {types: [], showIcon: true},
  fonts: defaultFonts,
  formats: {},
  settings: {PUBLIC_MAPS_API_KEY: ""}
}

const siteConfigLoadedStore = writable<boolean>(false);
const siteConfigStore = writable<SiteConfig>(defaultSiteConfig);
const siteConfigCopyStore = writable<SiteConfig>(defaultSiteConfig);

export const siteConfigLoaded = derived(siteConfigLoadedStore, e => e)

export const siteConfig = derived(siteConfigStore, e => e)
export const nav = derived(siteConfigStore, e => e.nav)
export const landing = derived(siteConfigStore, e => e.landing)
export const i18n = derived(siteConfigStore, e => e.i18n)
export const cookies = derived(siteConfigStore, e => e.cookies)
export const fonts = derived(siteConfigStore, e => e.fonts)
export const formats = derived(siteConfigStore, e => e.formats)
export const settings = derived(siteConfigStore, e => e.settings)

export const siteConfigChanged = derived([siteConfigStore, siteConfigCopyStore], ([e1, e2]) => {
  if(!e1 || !e2) return false;
  const changed = !deepEqual(e1,e2)
  console.log(changed, e1, e2)
  return changed
})

export const initSite = (siteConfig:SiteConfig) => {
  siteConfigStore.set(siteConfig)
  siteConfigCopyStore.set(JSON.parse(JSON.stringify(siteConfig)))
  siteConfigLoadedStore.set(true)
}

export const restoreSite = () => {
  siteConfigStore.update( () => {
    return JSON.parse(JSON.stringify(get(siteConfigCopyStore)))
  })
}

export const updateNav = (nav:Nav) => {
  siteConfigStore.update( (s:SiteConfig) => {
    s.nav = nav; 
    return s
  })
}


export const updateLanding = (l:Landing) => {
  siteConfigStore.update( (s:SiteConfig) => {
    s.landing = l; 
    return s
  })
}


export const updateI18n = (i:I18n) => {
  siteConfigStore.update( (s:SiteConfig) => {
    s.i18n = i; 
    return s
  })
}


export const updateAccos = (a:Acco[]) => {
  siteConfigStore.update( (s:SiteConfig) => {
    s.accos = a; 
    return s
  })
}


export const updateCookies = (c:CookieSettings) => {
  siteConfigStore.update( (s:SiteConfig) => {
    s.cookies = c; 
    return s
  })
}


export const updateFonts = (f:Fonts) => {
  siteConfigStore.update( (s:SiteConfig) => {
    s.fonts = f; 
    return s
  })
}

export const updateFormats = (f:Formats) => {
  siteConfigStore.update( (s:SiteConfig) => {
    s.formats = f; 
    return s
  })
}

export const updateSettings = (set:Settings) => {
  siteConfigStore.update( (s:SiteConfig) => {
    s.settings = set; 
    return s
  })
}

export const makeTranslationChangeHandler = ( key: string|undefined, translation: string) => {
  console.log("mvch called", key, translation)
  
  return function( event:CustomEvent ) {
    const newValue = event.detail.value

    console.log("mvch triggered", key, translation, newValue)
  
    if(key) {
      siteConfigStore.update( (s:SiteConfig) => {
        const t = s.i18n.translations[translation]
        if(t && t.site) t.site[key] = newValue
        return s
      })
    }
  }
}

export const siteT = (key: string|undefined, translation: string):string => {
  if(!key) return ""

  if(!get(siteConfigLoadedStore)) return ""

  const i18nContent = get(i18n)

  if(!i18nContent) return ""
  if(!i18nContent.translations) return ""

  const t = i18nContent.translations[translation]
  if(!t) return ""
  if(!t.site) return ""

  return t.site[key]
}

export const getLang = (short: string):Lang => {
  return Languages[short]
}
*/