export interface FontSpec {
  shortName: string
  cssName: string
  variant: string
  fallback: string
  fontsource: boolean
}

export interface Fonts {
  main: FontSpec
  nav: FontSpec
  pageTitle: FontSpec
  landingTitle: FontSpec
  header: FontSpec
}