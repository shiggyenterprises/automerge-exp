import type { I18n } from './i18n';
import type { Landing } from './landing';
import type { Nav } from './nav';
import type { Acco } from './accos';
import type { CookieSettings } from './cookies';
import type { Fonts } from './fonts';
import type { Formats } from './formats';
import type { Settings } from './settings';

import type { DateTime } from 'luxon';
import { DateTime as luxon } from 'luxon';


export const DefaultSiteConfigPaths = {
  navPath: 'src/lib/conf/nav.json',
  landingPath: 'src/lib/conf/landing.json',
  i18nPath: 'src/lib/conf/translations.json',
  accosPath: 'src/lib/conf/accos.json',
  cookiesPath: 'src/lib/conf/cookies.json',
  fontsPath: 'src/lib/conf/fonts.json',
  formatsPath: 'src/lib/conf/formats.json',
  settingsPath: 'src/lib/conf/settings.json',

  globalCssPath: 'static/global.css',
  favIconPath: 'static/favicon.ico'
}


export const DefaultFonts = {
  "main": {
    "shortName": "helvetica",
    "cssName": "Helvetica",
    "variant": "normal",
    "fallback": "sans-serif",
    "fontsource": false
  },
  "nav": {
    "shortName": "system-ui",
    "cssName": "system-ui",
    "variant": "small-caps",
    "fallback": "sans-serif",
    "fontsource": false
  },
  "pageTitle": {
    "shortName": "arizonia",
    "cssName": "Arizonia",
    "variant": "normal",
    "fallback": "sans-serif",
    "fontsource": true
  },
  "landingTitle": {
    "shortName": "raleway",
    "cssName": "Raleway",
    "variant": "normal",
    "fallback": "sans-serif",
    "fontsource": true
  },
  "header": {
    "shortName": "carrois-gothic",
    "cssName": "Carrois Gothic",
    "variant": "small-caps",
    "fallback": "sans-serif",
    "fontsource": true
  } 
}

export interface SiteConfig {
  nav: Nav
  landing: Landing
  i18n: I18n
  accos: Acco[]
  cookies: CookieSettings
  fonts: Fonts
  formats: Formats
  settings: Settings
  globalCss: string
  favIcon: string
}

export const DefaultSiteConfig:SiteConfig = {
  nav: {main: [], footer: []},
  landing: {headerPhoto: '', sections: []},
  i18n: {defaultLang: 'en', translations: {}},
  accos: [],
  cookies: {types: [], showIcon: true},
  fonts: DefaultFonts,
  formats: {},
  settings: {PUBLIC_MAPS_API_KEY: ''},
  globalCss: '',
  favIcon: ''
}

export interface SiteConfigDTO {
  user: string
  live: string
  editing: string
  created: string
  updated: string
}

export interface RenderSite {
  created: DateTime
  updated: DateTime
  user: string
  render: string
  url: string
}

export interface RenderSiteDTO {
  created: string
  updated: string
  user: string
  render: string
  url: string
}


export const mapRenderSite = (d:RenderSiteDTO):RenderSite => {
  return {
    created: luxon.fromISO(d.created),
    updated: luxon.fromISO(d.updated),
    render: d.render,
    url: d.url,
    user: d.user
  }
}
