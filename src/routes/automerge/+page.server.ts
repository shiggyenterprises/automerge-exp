export const ssr = false;
export const prerender = false;

import type  { PageServerLoad } from './$types'
import { fail, type Actions } from '@sveltejs/kit';

export interface AutomergeFile {
  id: string
  binary: Uint8Array|null
}

let file:AutomergeFile = {
  binary: null,
  id: 'd314dc7e-99f7-4256-bfbd-49939acab48f'
}

export const load = (() => {

  //await response.arrayBuffer()
  console.log("load called")
  return {
    file
  }

}) satisfies PageServerLoad;

export const actions = {
  upload: async ({request}) => {

    const data = await request.formData();
    const id = data.get('id') as string;

    console.log(id)

    if (id != file.id) {
      console.log('wrong id')
      fail(400) 
    }

		const configString =  data.get('config') as string;
    console.log(configString)

    if(!configString) {
      console.log('Missing config')
      fail(400)
    }
    
    const json = JSON.parse(configString)
    console.log(json)
    
    return {
      status: 'success'
    }
  }
} satisfies Actions